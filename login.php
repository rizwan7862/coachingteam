<?php
//session_start();
include('includes/header.php');
include('functions.php');  
?>


<link rel="stylesheet" type="text/css" href="css/style.css">

<div class="container">

<!-- Outer Row -->
<div class="row justify-content-center">

  <div class="col-xl-6 col-lg-6 col-md-6">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Login Here!</h1>
                <?php

                    if(isset($_SESSION['status']) && $_SESSION['status'] !='') 
                    {
                        echo '<h2 class="bg-danger text-white"> '.$_SESSION['status'].' </h2>';
                        unset($_SESSION['status']);
                    }
                ?>
              </div>

                <form class="user" method="post" action="login.php">

                  <?php echo display_error(); ?>

                  <div class="input-group">
                    <!-- <label>Username</label> -->
                    <input type="text" name="username" >
                  </div>
                  <div class="input-group">
                    <!-- <label>Password</label> -->
                    <input type="password" name="password">
                  </div>
                  <div class="input-group">
                    <button type="submit" class="btn" name="login_btn">Login</button>
                  </div>
                  <p>
                    Not yet a member? <a href="register.php">Sign up</a>
                  </p>
                </form>


            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>

</div>


<?php
include('includes/scripts.php'); 
?>